import * as React from 'react';
import { View, Text, Button, TextInput, Image } from 'react-native';
import { NavigationContainer  } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

function HomeScreen({ navigation, route }) {

  React.useEffect(() => {
    if (route.params?.post) {
      // Post updated, do something with `route.params.post`
      // For example, send the post to the server
    }
  }, [route.params?.post]);

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen </Text>

      <Button
        title="Create post"
        onPress={() => navigation.navigate('CreatePost')}
      />
      <Text style={{ margin: 10 }}>Post: {route.params?.post}</Text>

      <Button
        title="Go to Details"
        onPress={() => {
          navigation.navigate('Details',
            {

              otherParam: 'anything you want here',
            })
        }}
      />
      <Text>Go to Profile ... </Text>
      <Button
        title="Go to Profile"
        onPress={() =>
          navigation.navigate('Profile', { name: 'Custom header' })
        }
      />



      <Text>Updated ... </Text>
      <Button
        title="Update the title"
        onPress={() => navigation.setOptions({ title: 'Updated!' })}
      />  

      <Text>Go to Count Press ... </Text>
      <Button
        title="Go to Count"
        onPress={() =>
          navigation.navigate('CountPress')
        }
      />

       <Text>Go to Nesting Navigators ... </Text>
       <Button title="Go to Tab Navigators"
        onPress={() => navigation.navigate('NestingNavigators')}
        />

    </View>
  );
}

//------------------------


function CreatePostScreen({ navigation, route }) {
  const [postText, setPostText] = React.useState('');

  return (
    <>
      <TextInput
        multiline
        placeholder="What's on your mind?"
        style={{ height: 200, padding: 10, backgroundColor: 'white' }}
        value={postText}
        onChangeText={setPostText}
      />
      <Button
        title="Done"
        onPress={() => {
          // Pass and merge params back to home screen
          navigation.navigate({
            name: 'Home',
            params: { post: postText },
            merge: true,
          });
        }}
      />
    </>
  );
}

//-----------------------

const ProfileScreen = ({ route, navigation }) => {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Profile screen</Text>
      <Button title="Go back" onPress={() => navigation.navigate('Home')} />
    </View>
  );
}


//-----------------------

const DetailsScreen = ({ route, navigation }) => {

  const { itemId, otherParam } = route.params;

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>

      <Text>itemId: {JSON.stringify(itemId)}</Text>
      <Text>otherParam: {JSON.stringify(otherParam)}</Text>

      <Button
        title="Go to Details... again"
        onPress={() =>
          navigation.push('Details', {
            itemId: Math.floor(Math.random() * 100),
          })
        }
      />

      <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
      <Button title="Go back" onPress={() => navigation.goBack()} />
      <Button
        title="Go back to first screen in stack"
        onPress={() => navigation.popToTop()}
      />

    </View>
  );
}

//-------------------------------

const LogoTitle = ({ props }) => {
  return (
    <Image
      style={{ width: 50, height: 50 }}
      source={require('./assets/logo512.png')} />
  )
};

//------------------------------- 
const ButtonInfo = ({ props }) => {
  return (
    <Button
      title='Info'
      color='#7af'
      onPress={() => alert('This is a Button!')}
    />
  )
};


//------------------------------- 

  const CountPressScreen = ({ route, navigation }) => {
     
     const [count, setCount] = React.useState(0);

     React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button onPress={() => setCount(c => c + 1)} title="Update count" />
      ),
    });
  }, [navigation]);

    return( <Text style={ {marginTop: 15 }  } > Count: {count} </Text> );
  }
//------------------------------- 

  const NestingNavigatorsScreen = ({ route, navigation }) => {

    return (
    <Tab.Navigator>
      <Tab.Screen name="Feed" component={Feed} />
      <Tab.Screen name="Messages" component={Messages} />
    </Tab.Navigator>
  );

  }

//------------------------------- 
   const Feed = () => (<View></View> )

//------------------------------- 
   const Messages = () => (<View></View> )


//------------------------------- 



const Stack = createNativeStackNavigator();

const Tab = createBottomTabNavigator();  




function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#dfd',
          },
          headerTintColor: '#888',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} >

        <Stack.Screen name="Home"
          component={HomeScreen}
          options={{
            title: 'Home',
            headerTitle: (props) => <LogoTitle {...props} />,
            headerRight: () => (
              <Button
                title='Info'
                color='#7af'
                onPress={() => alert('This is a Button!')}
              />
            ),
          }}
        />

        <Stack.Screen name="Details"
          component={DetailsScreen}
          options={{ title: 'Overview',
                   headerRight: ({props})=>(<ButtonInfo {...props} />  ),
           }}
          initialParams={{ itemId: 1968 }}
        />

        <Stack.Screen name="CreatePost"
          component={CreatePostScreen}
          options={{ title: 'CreatePost' }}
        />

        <Stack.Screen name="Profile"
          component={ProfileScreen}
          options={({ route }) => ({ title: route.params.name })}
        />

        <Stack.Screen name="CountPress"
          component={CountPressScreen}
          options={ ( {route, navigation} ) => (
             { title: 'Count Press',
               headerTitle: (props) => <LogoTitle {...props} />,
              })}
        />

        <Stack.Screen name="NestingNavigators"
          component={NestingNavigatorsScreen}
          options={{title: 'Tab Navigators',
                    headerShown: false }}
        />            

      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
